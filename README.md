
<h1 align="center">Hi 👋, I'm <a href="https://simeononsecurity.com">SimeonOnSecurity</a></h1>
<h3 align="center">Cybersecurity Pro and Automation Enthusiast - Projects, Learning, Writing, and More</h3>
<div id="badges" align="center">
  <!-- Like Buttons -->
    <a rel=me  href="https://twitter.com/SimeonSecurity">
        <img src="https://img.shields.io/twitter/follow/SimeonSecurity?style=social" alt="Twitter">
          </a>
            <a rel=me href="https://github.com/simeononsecurity">
                <img src="https://img.shields.io/badge/GitHub-Follow-<COLOR>?logo=github&logoColor=white&color=blue" alt="GitHub">
                  </a>
                    <a rel=me href="https://infosec.exchange/@simeononsecurity">
                        <img src="https://img.shields.io/badge/Mastodon-Follow-<COLOR>?logo=mastodon&logoColor=white&color=blue" alt="Mastodon">
                          </a>
                            <a rel=me href="https://medium.com/@simeononsecurity">
                                <img src="https://img.shields.io/badge/Medium-Follow-<COLOR>?logo=medium&logoColor=white&color=black" alt="Medium">
                                  </a>
                                    <br>
                                      <!-- Discord -->
                                        <a href="https://discord.gg/CYVe2CyrXk">
                                            <img src="https://img.shields.io/discord/1077773186772521011?label=Cyber%20Sentinels%20Discord&logo=discord&logoColor=white" alt="Discord">
                                              </a>
                                                <a rel=me href="https://discord.gg/dwurqrfsAZ">
                                                    <img src="https://img.shields.io/discord/762530227511099432?label=SoS%20Discord&logo=discord&logoColor=white" alt="Discord">
                                                      </a>
                                                        <br>
                                                          
                                          
                                                                                                                                                            